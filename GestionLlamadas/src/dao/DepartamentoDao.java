package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import connectiondb.ConnectionDB;
import dto.Departamento;

public class DepartamentoDao {
	
	
	ConnectionDB con = new ConnectionDB();
	
	public static DepartamentoDao departamentodao= null;
	
	public static DepartamentoDao getInstans() {
		if(departamentodao==null) {
			departamentodao = new DepartamentoDao();
		}
		return departamentodao;
	}
	
	public Map<String,Departamento> selectDepartamento(){
		
		String sql = "select * from dbllamadas.departamento";
		Connection connection = con.getCon();
		
		Map<String, Departamento> listaDepartamentos = new HashMap<String,Departamento>();
		
		try {
			PreparedStatement ps= connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery(sql);
			
			while(rs.next()) {
				Departamento dep = new Departamento();
				String id = rs.getString("id");
				String descripcion = rs.getString("descripcion");
				
				dep.setId(Integer.parseInt(id));
				dep.setDescripcion(descripcion);
				
				listaDepartamentos.put(id, dep);
			}
			
			return listaDepartamentos;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	public boolean updateDepartamento() {
		
		return true;
	}

}
