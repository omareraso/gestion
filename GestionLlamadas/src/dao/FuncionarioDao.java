package dao;

import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import connectiondb.ConnectionDB;
import dto.Funcionario;

public class FuncionarioDao {
	
	ConnectionDB con = new ConnectionDB();
	
	private static FuncionarioDao funcionarioDao= null;
	
	public static FuncionarioDao getInstans () {
		if(funcionarioDao==null) {
			funcionarioDao = new FuncionarioDao();
		}
		return funcionarioDao;
	}
	
	public ArrayList<Funcionario> selectFuncionarios(){
		
		String sql = "select * from dbllamadas.funcionario";
		
		Connection connection = con.getCon();
		
		ArrayList<Funcionario> listaFuncionarios = new ArrayList<Funcionario>();
		
		try {
			PreparedStatement ps = connection.prepareStatement(sql);
			
			ResultSet result = ps.executeQuery(sql);
			
			while(result.next()) {
				
				Funcionario fun = new Funcionario();
				
				Integer id = Integer.parseInt(result.getString("id"));
				String nombre = result.getString("nombre");
				BigInteger numero_identificacion = new BigInteger(result.getString("numero_identificacion"));
				Integer departamento_id = Integer.parseInt(result.getString("departamento_id")); 
				
				fun.setNombre(nombre);
				fun.setNumero_identicicafion(numero_identificacion);
				fun.setId(id);
				fun.setDepartamento_id(departamento_id);
				
				listaFuncionarios.add(fun);
			}
			
			return listaFuncionarios;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	public boolean updateFuncionario(String id_funcionario, String departamento_id) {
		
		String sql = "update dbllamadas.funcionario set departamento_id = ? where id = ?";
		
		try {
			PreparedStatement ps = con.getCon().prepareStatement(sql);
			
			ps.setString(1, departamento_id);
			ps.setString(2, id_funcionario);
			ps.executeUpdate();
			
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}
	
	public boolean insertFuncionario(String nombre, String num_identificaicon, String departamento_id) {
		
		String sql = "insert into dbllamadas.funcionario (nombre, numero_identificacion, departamento_id) values (?,?,?)";
		
		try {
			PreparedStatement ps = con.getCon().prepareStatement(sql);
			
			ps.setString(1, nombre);
			ps.setString(2, num_identificaicon);
			ps.setString(3, departamento_id);
			
			ps.execute();
			
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

}
