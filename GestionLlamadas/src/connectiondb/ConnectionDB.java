package connectiondb;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class ConnectionDB {
	
	Connection con;
	
	public Connection getCon(){
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			
			Properties properties = new Properties();
			properties.setProperty("user", "root");
			properties.setProperty("password", "root");
			//properties.setProperty("useUnicode", "true");
			//properties.setProperty("useJDBCCompliantTimezoneShift", "true");
			//properties.setProperty("useLegacyDatetimeCode", "false");
			properties.setProperty("serverTimezone", "UTC");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/dbllamadas", properties);
			//con=DriverManager.getConnection("jdbc:mysql://localhost:3306/dbllamadas?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "root");
			//con=DriverManager.getConnection("jdbc:mysql://localhost:3306/dbllamadas", "root", "root");
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return con;
		
	}
	

}
