package dto;

import java.math.BigInteger;

public class Funcionario {
	
	private Integer id;
	private String nombre;
	private BigInteger numero_identificacion;
	private Integer departamento_id;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public BigInteger getNumero_identicicafion() {
		return numero_identificacion;
	}
	public void setNumero_identicicafion(BigInteger numero_identificacion) {
		this.numero_identificacion = numero_identificacion;
	}
	public Integer getDepartamento_id() {
		return departamento_id;
	}
	public void setDepartamento_id(Integer departamento_id) {
		this.departamento_id = departamento_id;
	}
}
