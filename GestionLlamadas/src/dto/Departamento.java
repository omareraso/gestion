package dto;

public class Departamento {
	
	private Integer id;
	private String descripcion;
	private Integer numero_funcionarios;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Integer getNumero_funcionarios() {
		return numero_funcionarios;
	}
	public void setNumero_funcionarios(Integer numero_funcionarios) {
		this.numero_funcionarios = numero_funcionarios;
	}

}
