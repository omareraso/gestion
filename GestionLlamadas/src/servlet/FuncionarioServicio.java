package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.ArrayList;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import dao.DepartamentoDao;
import dao.FuncionarioDao;
import dto.Funcionario;

@WebServlet("/FuncionarioServicio")
public class FuncionarioServicio extends HttpServlet{
	
	public FuncionarioServicio() {
		// TODO Auto-generated constructor stub
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) {
		
		try {
			ArrayList<Funcionario> listaFuncionarios = FuncionarioDao.getInstans().selectFuncionarios();
			
			String json = new Gson().toJson(listaFuncionarios);
			
			response.getWriter().write(json);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) {
		
		String tipo_operacion = request.getParameter("tipo_operacion");
		String id_funcionario = request.getParameter("id_funcionario");
		String departamento_id = request.getParameter("departamento_id");
		String nombre = request.getParameter("nombre");
		String num_identificacion = request.getParameter("num_identificacion");
		
		try {
			
			if(tipo_operacion.equalsIgnoreCase("insertar")) {
				
				Boolean insertado = FuncionarioDao.getInstans().insertFuncionario(nombre, num_identificacion, departamento_id);
				
				response.getWriter().write(insertado.toString());
			}else if(tipo_operacion.equalsIgnoreCase("actualizar")) {
				
				Boolean actualizado = FuncionarioDao.getInstans().updateFuncionario(id_funcionario, departamento_id);
				
				response.getWriter().write(actualizado.toString());
				
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
