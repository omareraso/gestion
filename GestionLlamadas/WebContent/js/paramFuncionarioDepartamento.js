angular.module('app', [])
.controller('paramfuncdep', ['$scope','$http', function($scope,$http) {
    
	$scope.listaDepartamentos = new Map();
	
	$scope.listaFuncionarios = [];
	
	$scope.ocultarAgregarFuncionario = true;
	
	$scope.nombre = "";
	$scope.num_identificacion = "";
	$scope.departamento_ins = {};
    
    $http({
        method : "GET",
        url : "http://localhost:8089/GestionLlamadas/DepartamentoServicio"
      }).then(function mySuccess(response) {
    	  $scope.listaDepartamentos = response.data;
    	  //console.log(response.data);
        }, function myError(response) {
          $scope.myWelcome = response.statusText;
      });
    
    $http({
        method : "GET",
        url : "http://localhost:8089/GestionLlamadas/FuncionarioServicio"
      }).then(function mySuccess(response) {
    	  $scope.listaFuncionarios = response.data;
    	  console.log(response.data);
        }, function myError(response) {
          $scope.myWelcome = response.statusText;
      });
    
    $scope.actualizarDepartamento = function(id_func,dep) {
    	console.log(dep);
    	$http({
            method : "POST",
            url : "http://localhost:8089/GestionLlamadas/FuncionarioServicio",
            params : {id_funcionario:id_func, departamento_id:dep.id,tipo_operacion:"actualizar"}
            	
          }).then(function mySuccess(response) {
        	   response.data;
        	   console.log(response.data);
            }, function myError(response) {
              $scope.myWelcome = response.statusText;
          });
    };
    
    $scope.ocultarMostrarAragregarFincionario = function(){
    	if($scope.ocultarAgregarFuncionario){
    		$scope.ocultarAgregarFuncionario=false;
    	}else{
    		$scope.ocultarAgregarFuncionario=true;
    	}
    }
    
    $scope.agregarFuncionario = function(){
    	$http({
            method : "POST",
            url : "http://localhost:8089/GestionLlamadas/FuncionarioServicio",
            params : {
            	nombre:$scope.nombre,
            	num_identificacion:$scope.num_identificacion, 
            	departamento_id:$scope.departamento_ins.id,
            	tipo_operacion:"insertar"}
            	
          }).then(function mySuccess(response) {
        	   response.data;
        	   console.log(response.data);
        	   
        	   $http({
        	        method : "GET",
        	        url : "http://localhost:8089/GestionLlamadas/FuncionarioServicio"
        	      }).then(function mySuccess(response) {
        	    	  $scope.listaFuncionarios = response.data;
        	    	  //console.log(response.data);
        	        }, function myError(response) {
        	          $scope.myWelcome = response.statusText;
        	      });
        	   
            }, function myError(response) {
              $scope.myWelcome = response.statusText;
          });
    }
    
    $scope.departamentoPorId = function(departamento_id){
    	console.log($scope.listaDepartamentos[departamento_id]);
    	return $scope.listaDepartamentos[departamento_id];
    }
}]);